﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTest
{
    public class WorkDayCalculator : IWorkDayCalculator
    {
        public DateTime Calculate(DateTime startDate, int dayCount, WeekEnd[] weekEnds)
        {
            var date = startDate.AddDays(dayCount - 1);
            if (weekEnds != null && weekEnds.Length != 0)
            {
                foreach (var weekEnd in weekEnds)
                {
                    if (weekEnd.StartDate < startDate)
                    {
                        if (!(weekEnd.EndDate < startDate))
                        {
                            date = date.AddDays(weekEnd.EndDate.Day - startDate.Day + 1);
                        }
                    }
                    else if (!(weekEnd.StartDate > date))
                    {
                        var weekDays = weekEnd.EndDate.Day - weekEnd.StartDate.Day;
                        date = date.AddDays(weekDays + 1);
                    }
                }
                return date;
            }
            return date;
        }

    }
}
